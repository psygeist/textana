from collections import Counter
from nltk.collocations import BigramCollocationFinder
import math
import nltk
import csv
import os

# change the base to tune idf calculation (log(num of docs/ (1 + num of
# occurrences of word)))
LOG_BASE = 10


def convert_tags(tag):
    if tag == "vbd" or tag == "vbg" or tag == "vbz":
        return 'v'
    else:
        return 'n'


class File(object):
    """ Read from file and Write to a file """

    def write_to_file(self, relFileName, content, mode='w'):
        with open(relFileName, mode) as fileWriteHandle:
            if isinstance(content[0], str):
                fileWriteHandle.write(' '.join(content))
            elif isinstance(content[0], tuple):
                fileWriteHandle.write('\n'.join('{} {}'.format(
                    tuples[0], tuples[1]) for tuples in content))

    def read_from_file(self, relFileName):
        with open(relFileName) as fileReadHandle:
            return fileReadHandle.read()

    def write_to_csv(self, refFileName, terms, docDict, dataKey, mode='w'):
        with open(refFileName, mode, newline='') as csvfile:
            spamwriter = csv.writer(csvfile, delimiter=',',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            spamwriter.writerow(["Filename"] + terms)
            for index in range(len(docDict)):
                temp = []
                temp.append(docDict[index]["name"])
                for (i, word) in enumerate(terms):
                    if word in docDict[index]["words"]:
                        temp.append(docDict[index][dataKey][word])
                    else:
                        temp.append(0)

                spamwriter.writerow(temp)


class TfIdf(object):

    def __init__(self):
        pass

    def tf_calculate(self, docList, totalWords=1):
        """ totalWords: can be provided for a normalized tf calculation.
            tf is calculated for each document by dividing the count of 
            word by the total number of word in the document        
        """
        counts = Counter(docList)
        tfDict = {}
        for word in docList:
            tfDict[word] = counts[word] / totalWords
        return (counts, tfDict)

    def idf_calculate(self, docDict, word, numOfDocs):
        """ LOG_BASE: default value is 10. Can be set to other values to change log base.
            idf is calculated for a word in a corpus(consisting of n documents) as, 
            number of docs / (1 + sum of existence of word in all docs)
        """
        idfDict = {}
        idfDict[word] = {"sum": sum(
            1 for key, value in docDict.items() if word in value["words"])}
        idfDict[word]["idf"] = math.log(
            numOfDocs / (1 + idfDict[word]["sum"]), LOG_BASE)
        return idfDict

    def tfidf_calculate(self, docDict, idfDict, key, idfSet):
        """tfidf for a word in a doc is, tf of word in a doc * idf of word in whole corpus
        """
        docDict[key]["tfidf"] = {}
        for word in idfSet:
            if word in docDict[key]["words"]:
                docDict[key]["tfidf"].update(
                    {word: docDict[key]["tf"][word] * idfDict[word]["idf"]})


class NGrams(object):

    def __init__(self):
        pass

    def bigram_nbest(self, wordList, scoreFun, NEWLINE_DELIMITER='\n', freq=2):
        """ scoreFun: can have these values BigramAssocMeasures.chi_sq, 
            .pmi, .liklihood_ratio, .poisson_stirling, .jaccard
            freq: bigrams with less than freq will not be listed in output file
        """
        bigramsObject = BigramCollocationFinder.from_words(wordList)
        bigramsObject.apply_freq_filter(freq)
        topScoreGrams = bigramsObject.nbest(scoreFun, 10)

        with open('pmi with freq cutoff as {}.txt'.format(freq), 'w') as f:
            for b in topScoreGrams:                
                if NEWLINE_DELIMITER not in list(b[0]):
                    f.write("{0} {1}\n".format(b[0], b[1]))

    def bigram_scores(self, wordList, scoreFun, NEWLINE_DELIMITER='\n', freq=1):
        
        bigramsObject = BigramCollocationFinder.from_words(wordList)
        bigramsObject.apply_freq_filter(freq)
        biGrams = bigramsObject.score_ngrams(scoreFun)

        with open('bigrams.txt'.format(freq), 'w') as f:
            for b in biGrams: 
                if NEWLINE_DELIMITER not in list(b[0]):
                    f.write("{0} {1}\n".format(b[0], b[1]))

class Entropy(object):
    def __init__(self):
        pass

    def entropy_calculate(self, labels):
        """Shannon Entropy
        """
        freqdist = nltk.FreqDist(labels)
        probs = [freqdist.freq(l) for l in freqdist]
        return -sum(p * math.log(p,2) for p in probs)

