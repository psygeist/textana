import nltk
from nltk.corpus import stopwords
from collections import Counter
import os
import re
import pprint

from utility import File
from replacer import skip_words, RegExReplacer
from utility import TfIdf

# initial config and initialization
pp = pprint.PrettyPrinter(indent=4)
INPUT_PATH = os.getcwd() + "/docs/"
stop = stopwords.words('english')
file = File()
regExReplacer = RegExReplacer()
tfIdf = TfIdf()

docDict = {}  # holds tf and tfidf results for each document
# frequency and tf (normalized: freq of word in document/total words in
# document) calculation
for index, filename in enumerate(os.listdir(INPUT_PATH)):
    rawText = file.read_from_file(INPUT_PATH + filename)

    # text pre processing
    sanitizedText = regExReplacer.replace(rawText)
    tokenizedText = nltk.word_tokenize(sanitizedText)
    normalizedText = [token.lower() for token in tokenizedText if token.lower(
    ) not in stop and token not in skip_words]
    docDict[index] = {"name": filename, "words": normalizedText}
    (docDict[index]["counts"], docDict[index]["tf"]) = tfIdf.tf_calculate(docDict[index][
        "words"])  # supply second argument, len(docDict[index]["words"]), if necessary


# creating set of words for all documents
idfSet = set()
for key, value in docDict.items():
    idfSet |= set(value["words"])

# idf (log(num of docs/ (1 + num of occurrences of word))) calculation
idfDict = {}  # holds idf results for each unique word in all docs
for word in idfSet:
    idfDict.update(tfIdf.idf_calculate(docDict, word, len(docDict)))

for key, value in docDict.items():
    tfIdf.tfidf_calculate(docDict, idfDict, key, idfSet)


terms = list(idfSet)
terms.sort()

file.write_to_csv("tf_matrix.csv", terms, docDict, "tf")

file.write_to_csv("tfidf_matrix.csv", terms, docDict, "tfidf")
