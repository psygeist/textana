import math
import os
import nltk
from nltk.corpus import stopwords
# custom imports
from utility import File, Entropy
from replacer import skip_words, RegExReplacer
from utility import File

INPUT_PATH = os.getcwd() + "/tweet/"
stop = stopwords.words('english')
file = File()
entropy = Entropy()
regExReplacer = RegExReplacer()

def processFile(fileName):
	rawText = file.read_from_file(INPUT_PATH + fileName)
	# text pre processing
	sanitizedText = regExReplacer.replace(rawText)
	tokenizedText = nltk.word_tokenize(sanitizedText)
	return [token.lower() for token in tokenizedText if token.lower(
	) not in stop and token not in skip_words]


randomTweets = processFile("random.txt", )
print(randomTweets)
entRandomTweets = entropy.entropy_calculate(randomTweets)
print(entRandomTweets)
file.write_to_file("Entropy_RandomTweets", randomTweets +  [str(entRandomTweets)])

spamTweets = processFile("spam.txt", )
print(spamTweets)
entSpamTweets = entropy.entropy_calculate(spamTweets)
print(entSpamTweets)
file.write_to_file("Entropy_SpamTweets", randomTweets + [str(entSpamTweets)])

mixedTweets = randomTweets + spamTweets
print(mixedTweets)
entMixedTweets = entropy.entropy_calculate(mixedTweets)
print(entMixedTweets)
file.write_to_file("Entropy_MixedTweets", mixedTweets + [str(entMixedTweets)])
