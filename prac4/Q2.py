from nltk.collocations import BigramCollocationFinder
from nltk.metrics import BigramAssocMeasures
from nltk import collocations
import nltk
from nltk.corpus import stopwords
import os
from matplotlib import pylab
# custom imports
from utility import File
from replacer import skip_words, RegExReplacer
from utility import NGrams


NEWLINE_DELIMITER = '\n'
INPUT_PATH = os.getcwd() + "/corpus/"
stop = stopwords.words('english')
file = File()
regExReplacer = RegExReplacer()
nGrams = NGrams()

corpus = []

for index, filename in enumerate(os.listdir(INPUT_PATH)):
    rawText = file.read_from_file(INPUT_PATH + filename)

    # text pre processing
    sanitizedText = regExReplacer.replace(rawText)
    tokenizedText = nltk.word_tokenize(sanitizedText)
    normalizedText = [token.lower() for token in tokenizedText if token.lower(
    ) not in stop and token not in skip_words]
    corpus.extend([NEWLINE_DELIMITER] + normalizedText)

print(corpus)
nGrams.bigram_scores(corpus, BigramAssocMeasures.pmi, NEWLINE_DELIMITER, freq=1)

nGrams.bigram_nbest(corpus, BigramAssocMeasures.pmi, NEWLINE_DELIMITER, freq=1)
nGrams.bigram_nbest(corpus, BigramAssocMeasures.pmi, NEWLINE_DELIMITER, freq=2)
nGrams.bigram_nbest(corpus, BigramAssocMeasures.pmi, NEWLINE_DELIMITER, freq=3)
nGrams.bigram_nbest(corpus, BigramAssocMeasures.pmi, NEWLINE_DELIMITER, freq=4)