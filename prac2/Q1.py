import nltk
from nltk.corpus import stopwords
from utility import File

# class instances
stop = stopwords.words('english')
file = File()


rawText = file.read_from_file('./corpus.txt')

tokenizedText = nltk.word_tokenize(rawText)
print("Tokenized Form \n {}".format(tokenizedText))
file.write_to_file('./Q1_tokenized.txt', tokenizedText)

normalizedText = [word.lower() for word in tokenizedText]

# stop words removal
# normalizedText = [i for i in normalizedText if i not in stop]
print("Normalized Form \n {}".format(normalizedText))
file.write_to_file('./Q1_normalized.txt', normalizedText)

posTagged = nltk.pos_tag(normalizedText)
print("POS tagged \n {}".format(posTagged))
file.write_to_file('./Q1_POStaggedTuples.txt', posTagged)
