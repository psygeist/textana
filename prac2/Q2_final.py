import nltk
import re
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
from utility import File, convert_tags
from replacer import SpellingReplacer, RegExReplacer, RepeatReplacer, skip_words

# class instances
stop = stopwords.words('english')
porterStemmer = PorterStemmer()
wordNetLemmatizer = WordNetLemmatizer()
spellReplacer = SpellingReplacer()
regExReplacer = RegExReplacer()
repeatReplacer = RepeatReplacer()
file = File()

rawText = file.read_from_file('./corpus.txt')
rawText = re.sub("[^a-zA-Z] ", " ", rawText)

rawNormalizedText = regExReplacer.replace(rawText.lower())

tokenizedText = nltk.word_tokenize(rawNormalizedText)

normalizedText = [word.lower()
                  for word in tokenizedText if word not in skip_words]

# spell replacer and repeat character rectifier
normalizedText = [spellReplacer.replace(i).lower() for i in normalizedText]
normalizedText = [repeatReplacer.replace(i) for i in normalizedText]

# porter stemmer
stemmed = [porterStemmer.stem(word) for word in normalizedText]
print("Stemmed Output \n {}".format(stemmed))
file.write_to_file('./Q2_final_PorterStemmed.txt', stemmed)

# wordnet lemmatizer
posTagged = nltk.pos_tag(normalizedText)
wordNetPOSTagged = [(item[0], convert_tags(item[1].lower()))
                    for item in posTagged]
lemmatized = [wordNetLemmatizer.lemmatize(
    tuples[0], tuples[1]) for tuples in wordNetPOSTagged]
print("Lemmatized Output \n {}".format(lemmatized))
file.write_to_file('./Q2_final_WordNetlemmatized.txt', lemmatized)
