import enchant
import re
from nltk.metrics import edit_distance
from nltk.corpus import wordnet

replacement_patterns = [(r'let\'s', 'let us'),
                        (r'i\'m', 'i am'),
                        (r'(\w+)\'d', '\g<1> would'),
                        (r'(\w+)n\'t', '\g<1> not'),
                        ]

skip_words = ['?', "'", '"', '.', '""',
              "''", ',', ';', '!', '-', ':', ' ', "``"]
replace_filter = skip_words + ["@"]


class RegExReplacer(object):
    """ Expands contractions for better tokenization.
            Normalize the text to all lowercase before using this."""

    def __init__(self, patterns=replacement_patterns):
        self.patterns = [(re.compile(regex), replacement)
                         for (regex, replacement) in patterns]

    def replace(self, text):
        contractions = text
        for (pattern, repl) in self.patterns:
            contractions = re.sub(pattern, repl, contractions)
        return contractions


class SpellingReplacer(object):
    """ Misspelled words (e.g.blabbur) are checked with enchant and then replaced with suggestions with max distance <=2.
            Otherwise the original word is kept as it is."""

    def __init__(self, dict_name="en", max_distance=1):
        self.spell_dict = enchant.Dict(dict_name)
        self.max_dist = max_distance

    def replace(self, word):
        if self.spell_dict.check(word) or word in replace_filter:
            return word
        suggestions = self.spell_dict.suggest(word)

        if suggestions and edit_distance(word, suggestions[0]) <= self.max_dist:
            return suggestions[0]
        else:
            return word


class RepeatReplacer(object):
    """ Unnecessary repeating set of characters (e.g.loooove) are removed.
            Wordnet search is done to ensure no real words (e.g.good) are removed."""

    def __init__(self, dict_name="en"):
        self.spell_dict = enchant.Dict(dict_name)
        self.repeat_regex = re.compile(r'(\w*)(\w)\2(\w*)')
        self.rep = r'\1\2\3'

    def replace(self, word):
        if wordnet.synsets(word) or self.spell_dict.check(word) or word in replace_filter or '.' in word or ' ' in word:
            return word
        replaced_word = self.repeat_regex.sub(self.rep, word)

        if replaced_word != word:
            return self.replace(replaced_word)
        else:
            return replaced_word
