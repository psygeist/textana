import nltk
import re
from nltk.stem import PorterStemmer
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
from utility import File, convert_tags


# class instances
stop = stopwords.words('english')
porterStemmer = PorterStemmer()
wordNetLemmatizer = WordNetLemmatizer()
file = File()

rawText = file.read_from_file('./corpus.txt')

tokenizedText = nltk.word_tokenize(rawText)

normalizedText = [word.lower() for word in tokenizedText]

# porter stemmer
stemmed = [porterStemmer.stem(word) for word in normalizedText]
print("Stemmed Output \n {}".format(stemmed))
file.write_to_file('./Q2_PorterStemmed.txt', stemmed)

# wordnet lemmatizer
posTagged = nltk.pos_tag(normalizedText)
wordNetPOSTagged = [(item[0], convert_tags(item[1].lower()))
                    for item in posTagged]
lemmatized = [wordNetLemmatizer.lemmatize(
    tuples[0], tuples[1]) for tuples in wordNetPOSTagged]
print("Lemmatized Output \n {}".format(lemmatized))
file.write_to_file('./Q2_WordNetlemmatized.txt', lemmatized)
