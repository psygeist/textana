def convert_tags(tag): 
    if tag == "vbd" or tag == "vbg" or tag == "vbz": 
        return 'v' 
    else: 
        return 'n'


class File(object):
    """ Read from file and Write to a file """

    def write_to_file(self, relFileName, content, mode='w'):
        with open(relFileName, mode) as fileWriteHandle:
            if isinstance(content[0], str):
                fileWriteHandle.write(' '.join(content))
            elif isinstance(content[0], tuple):
                fileWriteHandle.write('\n'.join('{} {}'.format(
                    tuples[0], tuples[1]) for tuples in content))

    def read_from_file(self, relFileName):
        with open(relFileName) as fileReadHandle:
            return fileReadHandle.read()
