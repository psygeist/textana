import nltk
import re
from nltk.corpus import stopwords
from utility import File
from replacer import SpellingReplacer, RegExReplacer, RepeatReplacer, skip_words

# class instances
stop = stopwords.words('english')
spellReplacer = SpellingReplacer()
regExReplacer = RegExReplacer()
repeatReplacer = RepeatReplacer()
file = File()


rawText = file.read_from_file('./corpus.txt')
rawText = re.sub("[^a-zA-Z] ", " ", rawText)

rawNormalizedText = regExReplacer.replace(rawText.lower())


tokenizedText = nltk.word_tokenize(rawNormalizedText)
print("Tokenized Form \n {}".format(tokenizedText))
file.write_to_file('./Q1_final_tokenized.txt', tokenizedText)

normalizedText = [word.lower()
                  for word in tokenizedText if word not in skip_words]
# spell replacer and repeat character rectifier
normalizedText = [spellReplacer.replace(i).lower() for i in normalizedText]
normalizedText = [repeatReplacer.replace(i) for i in normalizedText]
# stop words removal
# normalizedText = [i for i in normalizedText if i not in stop]
print("Normalized Form \n {}".format(normalizedText))
file.write_to_file('./Q1_final_normalized.txt', normalizedText)

posTagged = nltk.pos_tag(normalizedText)
print("POS tagged \n {}".format(posTagged))
file.write_to_file('./Q1_final_POStaggedTuples.txt', posTagged)
