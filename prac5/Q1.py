from utility import Similarity, Colors
from itertools import combinations

Similarity = Similarity()

targets = [ "two ate two ate door door", "roy ate two",
           "roy ate", "ate two", "roy two", "ate two"]

Similarity.jaccard_index_distance(targets)
Similarity.dice_coefficient_difference(targets)

