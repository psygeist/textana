__author__ = 'user'
# bits from http://stackoverflow.com/questions/15173225/how-to-calculate-cosine-similarity-given-2-sentence-strings-python
# load_docs, process_docs and compute_vector by MK
import math
import scipy
from collections import Counter

vector_dict = {}  # Dict that will hold tf-idf matrix

# Just loads in all the documents
def load_docs():
    print("Loading docs...")
    doc1 = ('d1', """I think SpaceX proved that camera on anything in space is worth it""")     
    doc2 = ('d2', """The SpaceX Falcon explosion was caused by a problem with the spacecraft""")     
    # doc3 = ('d3', """spacex camera ocean our application process""")     
    # doc4 = ('d4', """spacex camera ocean""")     
    doc3 = ('d3', """SpaceX keep focusing on ocean landings""")     
    doc4 = ('d4', """SpaceX keep focusing on ocean landings again""")     
    doc5 = ('d5', """SpaceX keep focusing on ocean landings again and again""")     
    doc6 = ('d6', """SpaceX keep focusing on ocean landings again and again after retrials""")     
    doc7 = ('d7', """SpaceX keep focusing on ocean landings again soon after trials""")     
    doc8 = ('d8', """SpaceX keep focusing on ocean landings again immediately after trials""")

    return [doc1, doc2, doc3, doc4, doc5, doc6, doc7, doc8]

# Computes TF for words in each doc, DF for all features in all docs;
# finally whole Tf-IDF matrix
def process_docs(all_dcs):
    stop_words = ['of', 'and', 'on', 'in']
    all_words = []  # list to collect all unique words in each docs
    counts_dict = {}  # dict to collect doc data, word-counts and word-lists
    for doc in all_dcs:
        words = [x.lower() for x in doc[1].split() if x not in stop_words]
        words_counted = Counter(words)  # counts words in a doc
        # list of the unique words in the doc
        unique_words = list(words_counted.keys())
        # make dict entry {'d1' : {'a': 1, 'b':6}}
        counts_dict[doc[0]] = words_counted
        # collect all unique words from each doc; bit hacky
        all_words = all_words + unique_words
    # print(all_words)
    n = len(counts_dict)  # number of documents is no of entries in dict
    # DF of all unique words from each doc, counted
    df_counts = Counter(all_words)
    # computes TF-IDF for all words in all docs
    compute_vector_len(counts_dict, n, df_counts)


# computes TF-IDF for all words in all docs
def compute_vector_len(doc_dict, no, df_counts):
    global vector_dict
    for doc_name in doc_dict:  # for each doc
        # get all the unique words in the doc
        doc_words = doc_dict[doc_name].keys()
        wd_tfidf_scores = {}
        for wd in list(set(doc_words)):  # for each word in the doc
            # get the word-counts-dict for the doc
            wds_cts = doc_dict[doc_name]
            wd_tf_idf = wds_cts[
                wd] * math.log(no / df_counts[wd], 10)  # compute TF-IDF
            # store Tf-IDf scores with word
            wd_tfidf_scores[wd] = round(wd_tf_idf, 4)
        # store all Tf-IDf scores for words with doc_name
        vector_dict[doc_name] = wd_tfidf_scores
        # print("Doc Name {} --- {}".format(doc_name,vector_dict[doc_name]))


def get_cosine(text1, q):
    vec1 = vector_dict[text1]
    # print(vec1)
    vec2 = vector_dict[q]
    # print(vec2)
    intersection = set(vec1.keys()) & set(vec2.keys())
    # print(intersection)
    # NB strictly, this is not really correct, needs vector of all features
    # with zeros
    numerator = sum([vec1[x] * vec2[x] for x in intersection])
    sum1 = sum([vec1[x]**2 for x in vec1.keys()])
    sum2 = sum([vec2[x]**2 for x in vec2.keys()])
    denominator = math.sqrt(sum1) * math.sqrt(sum2)
    if not denominator:
        return 0.0
    else:
        return round(float(numerator) / denominator, 3)


# RUN THE DEFINED FNS
all_docs = load_docs()
process_docs(all_docs)

d1 = 'd1'
d2 = 'd2'
d3 = 'd3'
d4 = 'd4'
d5 = 'd5'
d6 = 'd6'
d7 = 'd7'
d8 = 'd8'

print("***Cosine (Original Code)***")
cosine = get_cosine(d1, d2)
print('Document (d1,d2) cosine: ', cosine)
cosine = get_cosine(d2, d3)
print('Document (d2,d3) cosine: ', cosine)
cosine = get_cosine(d3, d1)
print('Document (d3,d1) cosine: ', cosine)
cosine = get_cosine(d3, d4)
print('Document (d3,d4) cosine: ', cosine)
cosine = get_cosine(d3, d5)
print('Document (d3,d5) cosine: ', cosine)
cosine = get_cosine(d3, d6)
print('Document (d3,d6) cosine: ', cosine)
cosine = get_cosine(d3, d7)
print('Document (d3,d7) cosine: ', cosine)
cosine = get_cosine(d3, d8)
print('Document (d3,d8) cosine: ', cosine)


#adding additional code using SciPy to calculate Cosine Similarity and Euclidean Distance
totalWords = set(list(vector_dict['d1'].keys()) + list(vector_dict['d2'].keys()) + list(vector_dict['d3'].keys()) + list(vector_dict['d4'].keys(
)) + list(vector_dict['d5'].keys()) + list(vector_dict['d6'].keys()) + list(vector_dict['d7'].keys()) + list(vector_dict['d8'].keys()))


docsDict = {}
for key in vector_dict:
    for word in totalWords:
        if word not in vector_dict[key]:
            vector_dict[key][word] = 0


from collections import defaultdict
matrixDict = defaultdict(list)
matrixDict["column"] = ["filename"] + list(totalWords)


for key, value in vector_dict.items():
    for word in matrixDict["column"]:
        if word in vector_dict[key]:
            matrixDict[key].append(vector_dict[key][word])

from scipy import spatial
from sklearn.metrics.pairwise import cosine_similarity

print("\n***Cosine Similarity (SciPy function)***")
result1 = round(float(1 - spatial.distance.cosine(matrixDict["d1"], matrixDict["d2"])), 3)
print('Document (d1,d2) cosine: ', result1)
result1 = round(float(1 - spatial.distance.cosine(matrixDict["d2"], matrixDict["d3"])), 3)
print('Document (d2,d3) cosine: ', result1)
result1 = round(float(1 - spatial.distance.cosine(matrixDict["d3"], matrixDict["d1"])), 3)
print('Document (d3,d1) cosine: ', result1)
result1 = round(float(1 - spatial.distance.cosine(matrixDict["d3"], matrixDict["d4"])), 3)
print('Document (d3,d4) cosine: ', result1)
result1 = round(float(1 - spatial.distance.cosine(matrixDict["d3"], matrixDict["d5"])), 3)
print('Document (d3,d5) cosine: ', result1)
result1 = round(float(1 - spatial.distance.cosine(matrixDict["d3"], matrixDict["d6"])), 3)
print('Document (d3,d6) cosine: ', result1)
result1 = round(float(1 - spatial.distance.cosine(matrixDict["d3"], matrixDict["d7"])), 3)
print('Document (d3,d7) cosine: ', result1)
result1 = round(float(1 - spatial.distance.cosine(matrixDict["d3"], matrixDict["d8"])), 3)
print('Document (d3,d8) cosine: ', result1)

print("\n ***Euclidean Distance (SciPy function)***")
distance = round(float(spatial.distance.euclidean(matrixDict["d1"], matrixDict["d2"])), 3)
print('Document (d1,d2) euclidean: ', distance)
distance = round(float(spatial.distance.euclidean(matrixDict["d2"], matrixDict["d3"])), 3)
print('Document (d2,d3) euclidean: ', distance)
distance = round(float(spatial.distance.euclidean(matrixDict["d3"], matrixDict["d1"])), 3)
print('Document (d3,d1) euclidean: ', distance)
distance = round(float(spatial.distance.euclidean(matrixDict["d3"], matrixDict["d4"])), 3)
print('Document (d3,d4) euclidean: ', distance)
distance = round(float(spatial.distance.euclidean(matrixDict["d3"], matrixDict["d5"])), 3)
print('Document (d3,d5) euclidean: ', distance)
distance = round(float(spatial.distance.euclidean(matrixDict["d3"], matrixDict["d6"])), 3)
print('Document (d3,d6) euclidean: ', distance)
distance = round(float(spatial.distance.euclidean(matrixDict["d3"], matrixDict["d7"])), 3)
print('Document (d3,d7) euclidean: ', distance)
distance = round(float(spatial.distance.euclidean(matrixDict["d3"], matrixDict["d8"])), 3)
print('Document (d3,d8) euclidean: ', distance)

