from nltk.metrics.distance import edit_distance
import distance

#  transposition flag allows transpositions edits (e.g., “ab” -> “ba”),

#first 5 tweets are user tweets
#after that all tweets are spams based on 5th tweet
s = [  "@UNTRESOR J.R.R. Tolkien's full name is Jolkien Rolkien Rolkien Tolkien",
	   "@ComedicBust As soon as I get to a party, I start saying goodbye; that way I'm out of there within 4 hours",
	   "@DanMentos On a positive note, once Trump becomes president and burns the world to the ground, our student loan debt is essentially wiped clean",
	   "@cynthiajones11 I am at my most Michael Phelps when I find out someone has peed in the pool", 
	   '@Rush_hour_swagg Hungry for some pizza? Have dominos and get a coupon here: dominos.com',
	   '@pizza_guy Hungry for some pizza? Have #dominos and get a coupon here: bit.ly/HtgVOn#432by',
	   '@hot_dogg Hungry for some pizza? Have dominos and #getacoupon here: bit.ly/HtgVOn#4trby',
	   '@snoop_lion Hungry for some pizza? Have dominos and get a coupon here: bit.ly/HtgVOn#H36b7',
	   '@razor_bmw Hungry for some pizza? Have dominos and get a coupon here #ilovedomino: bit.ly/HtgVOn#KIrby',
	   '@Irene_m Hungry for some pizza? Have dominos and get a coupon here fast: bit.ly/HtgVOn#E3RL0',
	   'Hungry for some pizza? Have dominos and get a coupon here: bit.ly/HtgVOn#eq78i',
	   '@audislave Hungry for some pizza? Have dominos and get a coupon here: bit.ly/HtgVOn#qwE32',
	   '@Muse Hungry for some pizza? Have dominos and get a coupon here: bit.ly/HtgVOn#hL5S6',
	   '@official_coldplay Hungry for some pizza now? bit.ly/HtgVOn#Cf1H8',
	   '@Jeff_Buckley Hungry for some pizzaaaa? Have dominos and get a coupon here: bit.ly/HtgVOn#M4dH7',
	   '@Super_Saiyan Have dominos and get a coupon here: bit.ly/HtgVOn#paF21',
	   '@Bio_hazard Hungry for some pizza? Have dominos and get a coupon here: bit.ly/HtgVOn#MG8iT',
	   '@marie_curie Hungry for some pizza? Have DOMINOS and get a coupon here: bit.ly/HtgVOn#lEeT3',
	   '@Albert_camus Hungry for some pizza? Have dominos and get a coupon here: bit.ly/HtgVOn#Hax0R',
	   '@Terry_pretchatt Hungry for some pizza? Have dominos and get a coupon here. Click here: bit.ly/HtgVOn#sPaM3',
	   '@Oscar_wilde Hungry for some pizza? Have dominos and get a coupon here. You wont believe: bit.ly/HtgVOn#da3ED',
	   '@jamce_joyce Hungry for pizza? Have dominos and get coupon here: bit.ly/HtgVOn#1L0Ve',
	   '@arthur_c_clarke Hungry for some pizza? Have dominos and get a coupon here: bit.ly/HtgVOn#lEg1T',
	   '@500_days_of_summer Hungry for some #pizza? Have dominos and get a coupon here: bit.ly/HtgVOn#eL1Te',
	   '@radiohead Hungry for some pizza? Have dominos: bit.ly/HtgVOn#s3InG',

	 ]

outPutList = []

for i, row in enumerate(s):
	temp = []
	for j, col in enumerate(s):
		temp.append(edit_distance(row, col, transpositions=False))
	outPutList.append(temp)


for i, row in enumerate(outPutList):
	print(row[i:])
# ans = edit_distance(s1, s2, transpositions=False)
# print(ans)

# ans = edit_distance(s3, s4, transpositions=False)
# print(ans)

# ans = edit_distance(s5, s6, transpositions=False)
# print(ans)

# ans = distance.levenshtein(s1, s2)
# print(ans)

# ans = distance.levenshtein(s3, s4)
# print(ans)

# ans = distance.levenshtein(s5, s6)
# print(ans)
# import matplotlib.pyplot as plt

# plt.xlim([0,26])
# plt.ylim([0,125])

# for i in range(25):
# 	x=[i for x in range(i+1)]
# 	y=outPutList[i][:i+1]
# 	print(x)
# 	print(y)
# 	plt.scatter(x, y, c=y, alpha=0.5)
	
# plt.show()